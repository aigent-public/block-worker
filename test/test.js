'use strict';

const Worker = require('../dist/index').Worker;

Worker.prototype.handleData = function (data) {
    this.emit('data', data);
};

const worker1 = new Worker('A');


let topic = 'topic/test';

worker1.on('data', (data) => {
    console.log(worker1.id, data);
});

const worker2 = new Worker('B');
worker2.on('data', (data) => {
    console.log(worker2.id, data);
});

worker1.handleData({test: 1});
worker1.handleData({test: 1});

worker2.handleData({test: 1});
worker2.handleData({test: 1});
