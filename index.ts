import {EventEmitter} from 'events';

export abstract class Worker extends EventEmitter {

    id: string;

    protected constructor(id: string) {
        super();
        this.id = id;
    }

    /**
     *  The handler will be implemented on each block
     *  data is returned through this.emit('data',data);
     *  Logging, caching and Monitoring should be handled by the handler.js
     */

    abstract handleData(data: object): void;

}
